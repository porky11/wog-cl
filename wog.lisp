(defpackage #:wog
  (:use #:cl #:vec)
  (:shadow #:restart))

(in-package #:wog)

(defparameter *balls* nil)

(defparameter *items* nil)

(defparameter *control* nil)

(defparameter *view-ball* #(0 0))

(defparameter *view* #(0 0))
(defparameter *rot* #(0 1))
(defparameter *zoom* 1.0)

(defparameter *info-text* "Use the mouse to move objects")

(defparameter *window-size* (vector-of 'fixnum 1280 768))

(defparameter *points* 0)

(defparameter *marked* nil)
(defparameter *hold* nil)
(defparameter *connect* nil)

(defparameter *connections* nil)

(defparameter *gravity* nil)
(defparameter *graviton* nil)

(defstruct ball
  (pos (error ":pos required~%"))
  (frc (vector-of 'float 0.0 0.0))
  (rad 0)
  (mass 1)
  (hard 1)
  (strength 1)
  (energy 1)
  (life 1)
  (connection-number 0)
  (connection-strength 1)
  (absorb nil)
  (gravity 0)
  (graviton 0)
  (item nil)
  (item-strength 1)
  (item-kill nil)
  (click-event nil)

  (connections nil)
  (reserve 0)
  (acc (vector 0 0)) (dmg 0) deleted)

(defun gravitate (ball planet)
  (with-slots (pos (grav graviton) mass) ball
    (with-slots ((pos0 pos) (grav0 gravity)) planet
      (let* ((disvec (v- pos0 pos))
             (dis (absvec disvec)))
        (when (< 0 dis)
          (let* ((g (* grav grav0 mass))
                 (fac (v* disvec (/ g (* dis dis dis)))))
            (accelerate ball fac)))))))

(defun gravity ()
  (loop for ball in *graviton*
     do (loop for planet in *gravity*
           do (gravitate ball planet))))

(defstruct connection
  length strength ball ball0 absorb)

(defun connect (ball ball0 length strength &optional absorb)
  (with-slots (connection-number connections) ball0
    (loop for conn in connections
       do (when (eq (connection-ball0 conn) ball)
            (setf *connections* (delete conn *connections*))
            (setf connections (delete conn connections))
            (return-from connect))))

    (with-slots (connection-number connections rad) ball
    (loop for conn in connections
       do (when (eq (connection-ball0 conn) ball0)
            (setf *connections* (delete conn *connections*))
            (setf connections (delete conn connections))
            (return-from connect)))
            
    (when (< (length connections) connection-number)
      (let ((conn (make-connection
                   :length (+ rad (ball-rad ball0) length) :strength  strength :ball ball :ball0 ball0 :absorb absorb)))
        (push conn *connections*)
        (push conn connections)))))

(defstruct item
  (pos (error ":pos required~%"))
  (kill nil)
  (effect nil)
  (strength nil)
  (ball nil)

  (size 1)
  )

(defun add-item (&rest args
                 &aux (item (apply #'make-item args)))
  (push item *items*))

(defun add-ball (self &rest args &key gravity graviton &allow-other-keys
                 &aux (ball (apply #'make-ball args)))
  (push ball *balls*)
  (when gravity (push ball *gravity*))
  (when graviton (push ball *graviton*))
  (when self (push ball *control*))
  ball)

(defparameter *mouse* (vector-of 'fixnum 0 0))

(defun real-pos (pos)
  (let* ((vpos (v- pos *view*)))
    (vector-bind (x y) vpos
      (vector-bind (s c) *rot*
        (let ((rpos (v+ (v* vpos c) (vector (* y s) (- (* x s))))))
          (mapvec #'round (v+ (v/ *window-size* 2) (v* rpos *zoom*))))))))

(defun act-ball (ball)
  (with-slots (pos frc rad mass acc dmg reserve deleted hard energy life item item-strength item-kill) ball
    (setf dmg (* 0.95 dmg))
    (let ((d (absvec acc))
          ;;(stable (* #.(expt 2 16) (/ hard) (/ mass (* rad rad))))
          )
      (incf dmg (* d d)))
    (incf reserve (* 0.01 (- energy reserve)))
    (when (< life dmg)
      ;;(format t "acc ~a, dmg  ~a~%" acc dmg)
      (when item
        (if (functionp item)
            (funcall item)
            (add-item :pos pos :effect item :strength item-strength :ball ball :kill item-kill)))
      (setf *balls* (delete ball *balls*)
            *hold* (unless (eq ball *hold*) *hold*)
            *connect* (unless (eq ball *connect*) *connect*)
            *control* (delete ball *control*)
            deleted t))
    (incv frc acc)
    (setv acc (vector 0 0))
    (incv pos (v/ frc mass))
    (mulv frc 0.95)
    (mapv #'round pos)
    ))

(defun act-connection (conn)
  (with-slots (strength length ball ball0 absorb) conn
    (with-slots (pos deleted connections reserve energy) ball
      (with-slots ((pos0 pos) (deleted0 deleted) (res0 reserve)) ball0
        (if (or deleted deleted0)
            (setf *connections* (delete conn *connections*)
                  connections (delete conn connections))
            (let* ((disvec (v- pos0 pos))
                   (dis (absvec disvec))
                   (dir (unitvec disvec))
                   (willdis (- length dis))
                   (frcvec (v* dir willdis strength)))
              (when absorb
                (let ((diff (min res0 (- energy reserve))))
                  (incf reserve diff)
                  (decf res0 diff)))
              (accelerate ball0 frcvec)
              (accelerate ball (v- frcvec))))))))

(defun hold-ball (ball)
  (with-slots (pos (frc0 frc) (mass0 mass)) ball
    (let* ((frc (v* (v- *mouse* (real-pos pos))))
           (dis (absvec frc))
           (dir (unitvec frc)))
      (loop for control in *control*
         do (with-slots (strength reserve dmg energy life acc frc mass) control
              (let* ((l (- life dmg))
                     (f2 (/ (- 1 (/ reserve energy) (/ l life)) 2))
                     (f (if (< 0 (- f2)) (sqrt (- f2)) 0))
                     (strn (* (min (/ dis 256) 1) f (min (sqrt life) (sqrt energy))))
                     (frc (v* dir strn)))
                #+nil
                (let* ((vel (v/ frc mass))
                       (vel0 (v/ frc0 mass0))
                       (dvel (v- vel vel0))))
                (decf reserve (* strn strn)) ;;TODO: physical
                (accelerate ball frc)
                (accelerate control (v- frc))))))))

(defun draw-ball (ball)
  (with-slots (pos rad) ball
    (sdl:draw-circle (real-pos pos) (round (* *zoom* rad)) :color sdl:*white*)
   ))

(defun item-color (effect n)
  (case effect
    (:rad (sdl:color :r n))
    (:energy (sdl:color :r n :g n))
    (:life (sdl:color :g n))
    (:mass (sdl:color :r (/ n 2) :g (/ n 2) :b (/ n 2)))
    (:connection-number (sdl:color :r n :b n))
    (:connection-strength (sdl:color :b n))
    ((nil) (sdl:color :g n :b n))
    (otherwise (sdl:color :g (/ n 2)))))


(defun draw-item (item)
  (with-slots (pos size effect) item
    (sdl:draw-filled-circle (real-pos pos) (round (* *zoom* (* 16 size size)))
                            :color (item-color effect 128)
    )))

(defun act-item (item)
  (with-slots (size effect kill strength ball) item
    (decf size 1/32)
    (case effect
      (:rad (loop for ball in *control*
               do (with-slots (rad) ball
                    (incf rad (/ strength 32)))))
      (:energy (loop for ball in *control*
               do (with-slots (energy) ball
                    (incf energy (/ strength 32)))))
      (:life (loop for ball in *control*
               do (with-slots (life) ball
                    (incf life (/ strength 32)))))
      (:connection-strength (loop for ball in *control*
                               do (with-slots ((str connection-strength)) ball
                                    (incf str (/ strength 32)))))
      (:mass (loop for ball in *control*
               do (with-slots (mass life energy (str connection-strength)) ball
                    (incf mass (/ strength 32))))))
    (with-slots (energy life) (car *control*)
      (format t "energy ~a, life ~a~%"
              energy life))
    (when (<= size 0)
      (case effect
        (:connection-number (loop for ball in *control*
                               do (with-slots ((num connection-number)) ball
                                    (incf num strength)))))
      (incf *points* 1)
      (when kill (funcall kill ball))
      (setf *items* (delete item *items*)))))



(defun draw-ball-stat (ball)
  (with-slots (pos rad dmg reserve energy life item) ball
    (sdl:draw-filled-circle (real-pos pos) (round (* (* *zoom* rad) (/ reserve energy)))
                            :color (item-color item 255))
    (sdl:draw-filled-circle (real-pos pos) (round (* (* *zoom* rad) (/ dmg life))) :color sdl:*blue*)
   ))

(defun draw-marked (ball)
  (with-slots (pos frc rad) ball
    (sdl:draw-circle (real-pos pos) (round (* *zoom* rad)) :color sdl:*yellow*)
    ))

(defun draw-hold (ball)
  (with-slots (pos frc rad) ball
    (sdl:draw-circle (real-pos pos) (round (* *zoom* rad)) :color sdl:*red*)
    ))

(defun draw-connect (ball)
  (with-slots (pos frc rad connections connection-number) ball
    (when (< (length connections) connection-number)
      (let* ((rp (real-pos pos))
             (dir (unitvec (v- rp *mouse*)))
             (posr (v+ rp (v* dir (- rad)))))
        (sdl:draw-line (mapv #'round posr) *mouse* :color sdl:*red*)
        ))))


(defun accelerate (ball vec)
  (with-slots (frc acc) ball
    (incv acc vec)
    )
  )

(defun draw-connection (conn)
  (with-slots (ball ball0) conn
    (with-slots (pos rad) ball
      (with-slots ((pos0 pos) (rad0 rad)) ball0
        (let* ((dir (unitvec (v- pos pos0)))
               (posr (v+ pos (v* dir (- rad))))
               (posr0 (v+ pos0 (v* dir rad0)))
               (rp (real-pos posr))
               (rp0 (real-pos posr0)))
          (sdl:draw-line rp rp0 :color sdl:*white*)
          )))))

(defun collide-balls (ball ball0)
  (with-slots (pos rad mass hard) ball
    (with-slots ((pos0 pos) (rad0 rad) (mass0 mass) (hard0 hard)) ball0
      (let* ((disvec (v- pos0 pos))
             (dis (absvec disvec))
             (mindis (+ rad rad0)))
        (when (< 0 dis mindis)
          (let* ((dir (v/ disvec dis))
                 (acc (- mindis dis))
                 (mfac (/ (* mass mass0) (+ mass mass0)))
                 (accvec (v* dir acc))
                 (force (v* accvec mfac hard hard0)))
            (accelerate ball0 force)
            (accelerate ball (v- force))))))))



(defparameter *left* 0)
(defparameter *right* 0)
(defparameter *up* 0)
(defparameter *down* 0)

(defparameter *hor* 0)
(defparameter *ver* 0)

(defun control-ball (ball)
  (with-slots (life energy reserve dmg) ball
    (let* ((l (- life dmg))
           (f2 (/ (- 1 (/ reserve energy) (/ l life)) 2))
           (f (if (< 0 (- f2)) (sqrt (- f2)) 0))
           (sfac (* f (sqrt (min life energy)))))
      
      (decf reserve (* (max *left* *right* *up* *down*) sfac sfac)) ;;TODO: physical
      (accelerate ball (v* (unitvec (vector (- *right* *left*) (- *down* *up*))) sfac))))) ;;TODO: physical

(defun delete-outside ()
  (setf *balls*
        (loop for ball in *balls*
           if
             (with-slots (pos click-event) ball
               (let ((rp (real-pos pos)))
                 (vector-bind (x y) rp
                   (vector-bind (w h) *window-size*
                     (and (< 0 x w)
                          (< 0 y h))))))
           collect ball
           else
           do (setf (ball-deleted ball) t))))


(defun run ()
  (gravity)
  ;;(setf *rot* (unitvec (v- *mouse* (v/ *window-size* 2))))
  (let ((zoom)
        (fac 1))
    (setf zoom (/ (/ (+ (aref *window-size* 0) (aref *window-size* 1)) 8)
                  (+ 32 (absvec (v- *mouse* (v/ *window-size* 2))))
                  ))
    (incf *zoom* (* zoom (- 1 fac)))
    (setf *zoom* (* *zoom* fac)))

  (let ((fac 0.95))
    (with-slots (pos) *view-ball*
      (mulv *view* fac)
      (incv *view* (v* pos (- 1 fac)))))

  
  (setq *marked* nil)
  (loop with mindis = nil
     for ball in *balls*
     do (with-slots (pos rad) ball
          (let ((dis  (distance *mouse* (real-pos pos))))
            (when  (and (< dis (* *zoom* rad))
                        (or (not mindis) (< dis mindis)))
              (setq *marked* ball
                    mindis dis)))))
  (let ((some-ball))
    (unless (< 1
               (loop for ball in *balls*
                  sum (with-slots (pos click-event) ball
                        (when click-event (return 2))
                        (let ((rp (real-pos pos)))
                          (vector-bind (x y) rp
                            (vector-bind (w h) *window-size*
                              (if (and (< 0 x w)
                                       (< 0 y h))
                                  (progn (setf some-ball ball) 1)
                                  0)))))))
      (setf (ball-click-event some-ball) #'restart)))
  

  (loop for ball in *balls*
     ;do (setf (ball-acc ball) (vector 0 0))
     do (act-ball ball))
  (loop for item in *items*
     do (act-item item))
  (loop for ball in *control*
     do (control-ball ball))
  (loop for conn in *connections*
     do (act-connection conn))
  (when *hold*
    (hold-ball *hold*))
  (loop for (ball . balls) on *balls*
     do (loop for ball0 in balls
           do (collide-balls ball ball0)))
  )



(defun draw ()
  ;;(sdl::clear-display sdl::*white*)
  (sdl:draw-box (sdl:rectangle-from-edges (vector 0 0) *window-size*) :color sdl:*black*)
  (loop for item in *items*
     do (draw-item item))
  (loop for ball in *balls*
     do (draw-ball-stat ball))
  (loop for ball in *balls*
     do (draw-ball ball))
  (when *marked*
    (draw-marked *marked*))
  (when *hold*
    (draw-hold *hold*))
  (when *connect*
    (draw-connect *connect*))
  (loop for conn in *connections*
     do (draw-connection conn))
  (sdl:draw-string-solid-* *info-text* 0 0 :color sdl:*white*)
  (sdl:draw-string-solid-* (format nil "~a" *points*) (aref *window-size* 0) 0 :color sdl:*white* :justify :right)
  (sdl:update-display)

  )

(let ((level 0))
      (defun next-level ()
        (incf level)
        (setf *reset*
              (case (mod level 6)
                (0 #'first-ball)
                (1 #'second-ball)
                (2 #'third-ball)
                (3 #'machine-level)
                (4 #'snake-level)
                (5 #'tentakel-level)
                ))
        (reset)))
    

(defun start ()
  (sdl:with-init (sdl:sdl-init-everything)
    (sdl:window (aref *window-size* 0) (aref *window-size* 1) :title-caption "Game")
    (sdl:initialise-default-font sdl:*font-9x18*)
    (sdl::clear-display sdl::*black*)
    (setf (sdl:frame-rate) 30)
    ;;(if (sdl::joystick-init-p) (error "init"))

    (sdl:with-events ()
      (:quit-event () t)
      (:key-down-event (:key key)
                       (case key
                         (:sdl-key-s (next-level))
                         (:sdl-key-escape (sdl:push-quit-event))
                         (:sdl-key-left (setf *left* 0))
                         (:sdl-key-right (setf *right* 0))
                         (:sdl-key-up (setf *up* 0))
                         (:sdl-key-down (setf *down* 0))))
      (:key-up-event (:key key)
                       (case key
                         (:sdl-key-left (setf *left* 0))
                         (:sdl-key-right (setf *right* 0))
                         (:sdl-key-up (setf *up* 0))
                         (:sdl-key-down (setf *down* 0))))
      (:mouse-motion-event (:x x :y y)
                           (setq *mouse* (vector-of 'fixnum x y)))
      (:video-resize-event (:w w :h h)
                           (sdl:window w h) 
                           (setq *window-size* (vector-of 'fixnum w h)))
      (:mouse-button-down-event (:button b)
                                (setf *info-text* "")
                                (when (eq b sdl:sdl-button-left)
                                  (when *marked*
                                    (with-slots ((event click-event)) *marked*
                                      (when event
                                        (funcall event *marked*)
                                        (setf event nil)))
                                    (setq *hold* *marked*)))
                                (when (eq b sdl:sdl-button-right)
                                  (setq *connect* *marked*)))
      (:mouse-button-up-event (:button b)
                              (when (eq b sdl:sdl-button-left) (setq *hold* nil))
                              (when (eq b sdl:sdl-button-right)
                                (when (and *marked* *connect*)
                                  (with-slots (absorb (pos0 pos) (rad0 rad) (strength connection-strength)) *connect*
                                    (with-slots (pos rad) *marked*
                                      (when (< (distance (real-pos pos) *mouse*) (* *zoom* rad))
                                        (let ((dis (distance pos pos0)))
                                          (when (< 0 dis)
                                            (connect *connect* *marked* (- dis rad rad0) strength absorb)))))))
                                  (setq *connect* nil)))
      (:idle ()
             (run)
             (draw)
             ))))

(defun castle (mass x y)
  (loop for i from 0 to 63
     for rot = (* i (/ pi 32))
     do (add-ball nil
                  :pos (v+ (vector x y) (v* (vector-of 'integer (sin rot)  (cos rot)) 331))
                  :frc (vector-of 'float 0 0)
                  :rad 16
                  :life #.(expt 2 14)
                  :hard 1
                  :item t
                  :mass mass)))

(defun hard-castle (mass x y)
  (loop for i from 0 to 63
     for rot = (* i (/ pi 32))
     do (add-ball nil
                  :pos (v+ (vector x y) (v* (vector-of 'integer (sin rot)  (cos rot)) 331))
                  :frc (vector-of 'float 0 0)
                  :rad 16
                  :life #.(expt 2 32)
                  :hard 1
                  :item t
                  :mass mass)))

#+nil
(add-ball t
          :pos (setf *view* (vector-of 'integer 0 0))
          :frc (vector-of 'float 0 0)
          :rad 32
          :hard 1
          :energy #.(expt 2 10)
          :life #.(expt 2 10)
          :mass #.(expt 2 0)
          :connection-number 4
          :connection-strength 1
          :strength 1)




#+light-balls
(loop for i from 1 to 0
   do (add-ball (vector-of 'integer (* i 32) (* i -128))
          (vector-of 'float 1.0 i)
          (* i 8)
          (* 8 8)))

(defun planet (x y)
  (add-ball nil
            :pos (vector-of 'integer x (+ #.(expt 2 10) y))
            :life #.(expt 2 32)
            :rad #.(expt 2 10)
            :frc (vector-of 'float 0 0)
            :mass #.(expt 2 24)))

(defun snake ()
  (let (last-ball
        (pos 512))
    (loop for i from 1 to 16
       do (let ((ball (add-ball nil
                                :pos (vector-of 'integer (incf pos (* i 8)) 0)
                                :rad (* 8 (sqrt i))
                                :connection-number 1
                                :absorb 1
                                :life #.(expt 2 20)
                                :energy #.(expt 2 16)
                                :item t
                                :mass i)))
            (when last-ball (connect ball last-ball (* 8 i) (/ i #.(expt 2 4)) #.(expt 2 12)))
            (setq last-ball ball)))
    (setf *view-ball* last-ball)
    (push last-ball *control*)))

(defun snake-level ()
  (setf *info-text* "Test: Snake. Many games can be made out of connected balls")
  (lake 64)
  (planet 2048 0)
  (snake)
  (castle 8 0 0))

(defun tenta-kel (x y)
  (let (last-ball
        (pos (- y 1224)))
    (loop for i from 1 to 16
       for j = (- 17 i)
       do (let ((ball (add-ball t
                                :pos (vector-of 'integer x (incf pos (* i 8)))
                                :frc (vector-of 'float 0 0)
                                :rad (* 4 (sqrt i))
                                :connection-number 2
                                :connection-strength i
                                :life #.(expt 2 24)
                                :energy #.(expt 2 12)
                                :mass (* i))))
            (when last-ball (connect last-ball ball (* 2 i) (/ i #.(expt 2 4)) #.(expt 2 12)))
            (setq last-ball ball)))
    last-ball))


(defun tentacle-monster (y)
  (let ((tentacles
         (loop for i from (- (* 3 -64) 32) to (+ (* 3 64) 32) by 64
            collect (tenta-kel i y)))
        
        (body (add-ball nil
                        :rad 128
                        :pos (vector-of 'integer 0 y)
                        :frc (vector-of 'float 0.0 0.0)
                        :click-event (lambda (ball)
                                       (declare (ignore ball))
                                       (setf *info-text* "Yo, you grabbed your head"))
                        :connection-number 4
                        :connection-strength 64
                        :life #.(expt 2 32)
                        :energy #.(expt 2 22)
                        :mass #.(expt 2 8))))
    (setf *view-ball* body) 
    (loop for tentacle in tentacles
       do (connect tentacle body 64 4 1))))

(defun lake (mass)
  (loop for x from -1024 to 1024 by 512
     do (loop for y from -1024 to 1024 by 512
           do (add-ball nil
                        :rad 64
                        :pos (vector-of 'integer x y)
                        :frc (vector-of 'float 0.0 0.0)
                        :connection-number 4
                        :connection-strength 64
                        :life #.(expt 2 32)
                        :energy #.(expt 2 16)
                        :hard 0.125
                        :mass mass))))

(defun machine ()
  (setf *info-text* "Test: This machine holds and stabilizes you, so you can move heavy balls without having mass yourself")
  (let ((self
         (add-ball t
                   :pos (vector-of 'integer 128 0)
                   :frc (vector-of 'float 0 0)
                   :connection-number 3
                   :rad 16
                   :mass 4
                   :life #.(expt 2 18)
                   :item #'reset
                   :energy #.(expt 2 16)))
        (a1
         (add-ball nil
                   :pos (vector-of 'integer 64 128)
                   :frc (vector-of 'float 0 0)
                   :connection-number 1
                   :rad 32
                   :mass 64
                   :life #.(expt 2 20)
                   :energy #.(expt 2 16)))
        (a2
         (add-ball nil
                   :pos (vector-of 'integer 64 -128)
                   :frc (vector-of 'float 0 0)
                   :connection-number 1
                   :rad 32
                   :mass 64
                   :life #.(expt 2 20)
                   :energy #.(expt 2 16)))
        (a3
         (add-ball nil
                   :pos (vector-of 'integer 256 0)
                   :frc (vector-of 'float 0 0)
                   :connection-number 1
                   :rad 32
                   :mass 64
                   :life #.(expt 2 20)
                   :energy #.(expt 2 16))))
    (setf *view-ball* self)
    (connect self a1 128 2)
    (connect self a2 128 2)
    (connect self a3 128 2)
    (connect a1 a2 256 1)
    (connect a2 a3 256 1)
    (connect a3 a1 256 1)))


(defun tentakel-level ()
  (setf *info-text* "This is the coolest test level. You are the tentacles.")
  (tentacle-monster 512)
  (hard-castle #.(expt 2 16) 0 -768)
  (planet 0 512)
  (lake 512)
  (tenta-kel -256 0) 0
  (tenta-kel 256 0))

(defun machine-level ()
  (lake 64)
  (planet 0 768)
  (machine))


;;(snake)
;;(castle 0 -758)

(defun level-3 (ball)
  (setf *info-text* "This is a planet")
  (delete-outside)
  (push ball *balls*)
  (setf (ball-deleted ball) nil)
  (setf *start* nil)
  (setf *reset* #'third-ball)
  (loop for i from 1 to 256
     do (connect (add-ball nil
                           :pos (v+ (ball-pos ball)
                                    (v* (vector (sin i) (cos i))
                                        (+ (- 1 (/ i (ball-rad ball))) (* (/ (1+ (mod i 32)) (ball-rad ball)) 256))
                                        (ball-rad ball)))
                           :life (* i i 16)
                           :item (if (integerp (/ i 2)) :life :energy)
                           :item-strength (if (integerp (/ i 2)) 1024 64)
                           :graviton 4
                           :connection-number 1
                           :connection-strength 1
                           :mass 4
                           :rad (* i))
                 ball
                 (* (1+ (mod i 32)) 128)
                 #.(expt 2 -4))))
                 
                           
                 
                 
  

(defun wall (x y)
  (add-ball nil
            :pos (vector-of 'integer x y)
            :mass 64
            :item t
            :life 16
            :rad 16))

(defun life-2 (l x y)
  (add-ball nil
            :pos (vector-of 'integer x y)
            :mass 4
            :life (* l l 4)
            :item :life
            :item-strength (* l l 4)
            :rad (* l 8)))


(defun level-2 (ball)
  (delete-outside)
  (setf *info-text* "You may find it in brighter green balls")
  (with-slots (pos) ball
    (add-ball nil
              :pos (incv (vector-of 'integer  0 -256) pos)
              :life 16
              :energy 1
              :rad 32
              :click-event (lambda (ball)
                             (declare (ignore ball))
                             (setf *info-text* "Watch out, not to hit walls until you found some life!"))
              :mass 64
              :hard 0.125)
    (vector-bind (x y) pos
      (loop for iy from (- y 512) downto (- y 1024) by 32
         do (wall (- x 48) iy)
         do (wall (+ x 48) iy))
      (loop for iy from (- y 768) downto (- y 1024) by 64
         do (life-2 1 x iy))

      (life-2 2 (- x 0) (- y 1024 128))
      (life-2 2 (- x 64) (- y 1024 256))
      (life-2 2 (+ x 64) (- y 1024 256))
      (life-2 2 (- x 128) (- y 1024 384))
      (life-2 2 (- x 0) (- y 1024 384))
      (life-2 2 (+ x 128) (- y 1024 384))

      (add-ball nil
                :pos (vector-of 'integer (- x 512 64) (- y 1024))
                :rad 512
                :mass 16
                :item t
                :graviton 1
                :life #.(expt 2 10))

      (add-ball nil
                :pos (vector-of 'integer (+ x 512 64) (- y 1024))
                :rad 512
                :mass 16
                :item t
                :graviton 1
                :life #.(expt 2 10))

      (castle #.(expt 2 5) x (- y 2048))

      (life-2 4 x (- y 2048 1024))
      (life-2 1 (+ x 128) (- y 2048 768))
      (life-2 1 (- x 128) (- y 2048 768))

      (flet ((mass (x y)
               (add-ball nil
                         :pos (vector-of 'integer x y)
                         :item :mass
                         :item-strength 1
                         :click-event (lambda (ball)
                                        (declare (ignore ball))
                                        (setf *info-text*
                                              "Killing ball will make you heavier. You won't be able to move that fast anymore and will take damage even at slower speed"))
                         :mass 1
                         :rad 16
                         :life 256)))
        (mass (+ x 64) (- y 2048))
        (mass (- x 64) (- y 2048))
        (mass x (- y 2048 64))
        (mass x (- y 2048 -64)))
      
      (loop for ix from (- x 512) downto (- x 1024) by 128
         do (add-ball nil
                      :pos (vector-of 'integer ix (- y 2048 64))
                      :item t
                      :mass 16
                      :rad 16
                      :life 64)
         do (add-ball nil
                      :pos (vector-of 'integer ix (- y 2048 (- 64)))
                      :item t
                      :mass 16
                      :rad 16
                      :life 64))
      (flet ((energy (l x y)
               (add-ball nil
                         :pos (vector-of 'integer x y)
                         :item :energy
                         :click-event (when (= 1 l)
                                        (lambda (ball)
                                          (declare (ignore ball))
                                          (setf *info-text*
                                                "Killing ball will give you energy. You will be able to move faster or longer")))
                         :mass 4
                         :rad (* l 16)
                         :life (* l l 16)
                         :item-strength (* l l)))
             (helpers (r x y)
               (add-ball nil
                         :pos (vector-of 'integer x y)
                         :item t
                         :mass 1
                         :rad r
                         :life 256)))
        (energy 1 (- x 1024 128) (- y 2048 32))
        (energy 1 (- x 1024 128) (- y 2048 -32))
        (energy 1 (- x 1024 256) (- y 2048))
        (energy 2 (- x 1024 384) (- y 2048 64))
        (energy 2 (- x 1024 384) (- y 2048 -64))
        (energy 2 (- x 1024 512) (- y 2048))

        (helpers 8 (- x 1024 768) (- y 2048 64))
        (helpers 16 (- x 1024 768) (- y 2048 128))
        (helpers 16 (- x 1024 768) (- y 2048))
        (helpers 16 (- x 1024 768) (- y 2048 -128))
        (helpers 8 (- x 1024 768) (- y 2048 -64))
        (helpers 32 (- x 1024 768) (- y 2048 256))
        (helpers 32 (- x 1024 768) (- y 2048 -256))
        
        (helpers 32 (- x 32) (- y 2048 1024 128))
        (helpers 32 (- x -32) (- y 2048 1024 128))
        (helpers 32 (- x 96) (- y 2048 1024 128))
        (loop for ix from (- x 128 -16) to (+ x 128 -16) by 32
           do (wall ix (- y 2048 1024 256)))

      (loop for i from 0 to 5
         do (progn
              (wall (+ x 512 (* 48 i)) (+ y -2048))
              (when (<= 3 i 4)
                  (wall (+ x 512 (* 48 i)) (- (+ y -2048) (- 128 (* 24 i))))
                  (wall (+ x 512 (* 48 i)) (+ y -2048 (- 128 (* 24 i)))))))

      (connect (add-ball nil
                         :pos (vector-of 'integer (+ x 1024 512) (+ -2048 y))
                         :item :connection-number
                         :connection-number 1
                         :item-strength 2
                         :life #.(expt 2 7)
                         :mass 16
                         :rad 64
                         :item-kill (lambda (ball)
                                      (setf *info-text*
                                            "Killing ball will increase the number of connections")
                                      (add-ball nil :pos (vector x y)
                                                :rad 16
                                                :life #.(expt 2 24)
                                                :click-event #'level-3)
                                      (vector-bind (x y) (ball-pos *view-ball*)
                                        (castle #.(expt 2 5) x y)))
                         :click-event (lambda (ball)
                                        (declare (ignore ball))
                                        (setf *info-text*
                                              "Killing this ball will increase the number of connections, you can create from you")))
               (helpers 32 (+ x 1024 512 64 32 128) (+ -2048 y)) 128 1/16)

      #|
      (incv (ball-pos *view-ball*) (vector 0 -1500))
      (incf (ball-life *view-ball*) 128)
      (setf (ball-energy *view-ball*) 128)
      (planet-2 *view-ball*)|#
))))

(defun planet-2 (ball)
  (setf *info-text* "Balls can also cause gravity?")
  (with-slots (pos) ball
    (add-ball nil
              :pos (v+ pos (vector 0 #.(expt 2 12)))
              :rad #.(expt 2 11)
              :mass #.(expt 2 64)
              :gravity #.(expt 2 20)
              :click-event #'level-3
              :hard (* 0.125 0.125)
              :life #.(expt 2 24))))

(defun route-2 (i)
  (if (< i 2)
      (lambda (ball)
        (with-slots (pos) ball
          (setf *info-text*
                (case i
                  (0 "Now you are bigger, but still get damage very fast")
                  (1 "You should search for extra life!")))
          (add-ball nil
                    :pos (incv (vector-of 'integer  0 -256) pos)
                    :life 16
                    :energy 1
                    :rad 32
                    :mass 64
                    :click-event (route-2 (1+ i))
                    :hard 0.125
                    )))
      #'level-2)
  )


(defun take-them (i)
  (if (< i 5)
      (lambda (ball)
        (with-slots (pos) ball
          (setf *info-text*
                (case i
                  (0 "Click the left mouse button above a ball to take it!")
                  (1 "Hold the button and move the mouse to accelerate it!")
                  (2 "You will be accelerated in the different direction")
                  (3 "Strong acceleration, for example at collisions, will kill a ball")
                  (4 "The damage of a ball is the blue ball inside it")))
          (add-ball nil
                    :pos (incv (vector-of 'integer  128 (* -64 i)) pos)
                    :life 16
                    :energy 1
                    :rad 32
                    :mass (expt 2 (- 6 i))
                    :click-event (take-them (1+ i))
                    :hard 0.125
                    )))
      (lambda (ball)
        (setf *info-text* "Killing balls of special color may give you new abilities")
        (with-slots (pos) ball
          (let ((p (incv (vector-of 'integer  128 (* -64 i)) pos)))
            (add-ball nil
                      :pos p
                      :life 3
                      :energy 1
                      :rad 32
                      :mass 1
                      :item :rad
                      :item-strength 8
                      :item-kill (lambda (ball)
                                   (setf *reset* #'second-ball)
                                   (setf *start* (route-2 0))
                                   (funcall
                                    (route-2 0)
                                    ball))
                      )
            (add-ball nil
                      :pos (v+ p (vector 64 0))
                      :life 1
                      :energy 1
                      :rad 16
                      :mass 1
                      :item t
                      )
            (add-ball nil
                      :pos (v- p (vector 64 0))
                      :life 1
                      :energy 1
                      :rad 16
                      :mass 1
                      :item t
                      )
            (add-ball nil
                      :pos (v+ p (vector 0 64))
                      :life 1
                      :energy 1
                      :rad 16
                      :mass 1
                      :item t
                      )
            (add-ball nil
                      :pos (v- p (vector 0 64))
                      :life 1
                      :energy 1
                      :rad 16
                      :mass 1
                      :item t
                      ))))))

(defun first-ball ()
  (setf *view-ball*
        (add-ball t
                  :pos (v* *view*)
                  :life 4
                  :energy 16
                  :rad 8
                  :graviton 4
                  :connection-strength 1/4
                  :mass 1
                  :click-event (take-them 0)
                  :item #'reset
            )))

(defun second-ball ()
  (setf *view-ball*
        (add-ball t
                  :pos (v* *view*)
                  :life 4
                  :energy 16
                  :rad 16
                  :graviton 4
                  :connection-strength 1/4
                  :mass 1
                  :click-event (route-2 0)
                  :item #'reset
            )))

(defun third-ball ()
  (setf *view-ball*
        (add-ball t
                  :pos (v* *view*)
                  :life (* 96 4)
                  :energy 128
                  :rad 16
                  :graviton 4
                  :connection-number 2
                  :connection-strength 1/4
                  :mass 4
                  :click-event #'planet-2
                  :item #'reset
            )))

(defparameter *start* (take-them 0))

(defparameter *reset* #'first-ball)

(defun restart (ball)
  (setf *info-text* "Press `S` to skip levels")
  (setf *balls* (list *view-ball*)
        *gravity* nil
        *graviton* nil
        *connections* nil
        *connect* nil
        *hold* nil
        *marked* nil)
  (funcall *start* ball))

(defun reset ()
  (setf *balls* nil
        *gravity* nil
        *graviton* nil
        *connections* nil
        *control* nil
        *connect* nil
        *hold* nil
        *marked* nil)
  (funcall *reset*))


(first-ball)


(start)

