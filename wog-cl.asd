(asdf:defsystem #:wog-cl
  :author "Fabio Krapohl <fabio.u.krapohl@fau.de>"
  :description ""
  :license "LLGPL"
  :depends-on (#:lispbuilder-sdl)
  :components ((:file "vectors")
               (:file "wog" :depends-on ("vectors"))))


